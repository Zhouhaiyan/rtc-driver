#include "i2cm.h"



static void i2cm_start ( void )
{	
	SDA_OUT();
	SDA_HIGH();  
	USDELAY(180);
	SCK_HIGH();   
	USDELAY(180);     
	SDA_LOW();   
	USDELAY(180); 		
	SCK_LOW(); 
	USDELAY(180);
}


static void i2cm_stop ( void )
{	
	SDA_OUT();
	SDA_LOW(); 
	USDELAY(180); 
	SCK_HIGH(); 
	USDELAY(180);
	SDA_HIGH(); 
	USDELAY(180);
}


static unsigned char i2cm_send_byte ( unsigned char send_data )
{
	unsigned char bit_cnt;
	unsigned char	b_ack=0;
	unsigned char i=200;
	
	SDA_OUT();
	for( bit_cnt=0; bit_cnt<8; bit_cnt++ ) 
	{ 
		SCK_LOW(); 
		if((send_data<<bit_cnt) & 0x80) 
			SDA_HIGH();  
		else 
			SDA_LOW();  
		USDELAY(180); 
		
		SCK_HIGH();       
		USDELAY(180);          
	}
	
	SCK_LOW();
	SDA_IN();
	USDELAY(180);
	
	SCK_HIGH();
	USDELAY(180);
	
	i = 200;
	while(i--)
	{
		USDELAY(180);
		if(READ_SDA()==0)
		{
			b_ack = 1;
			break;
		}  
	}
	
	SCK_LOW();
	USDELAY(180);
	return b_ack;
}


static unsigned char i2cm_read_byte ( void )
{
	unsigned char read_value=0;
	unsigned char bit_cnt;
	SDA_IN();
	for ( bit_cnt=0; bit_cnt<8; bit_cnt++ )
	{
		SCK_HIGH();       
		USDELAY(180);
		read_value <<= 1;
		if ( READ_SDA()==1 ) 
			read_value +=1;
		SCK_LOW();     
		USDELAY(180);
	}
	return (read_value);
}


static void i2cm_ack ( void )
{	
	SDA_OUT();
	SDA_LOW();   
	USDELAY(180);
	SCK_LOW();
	USDELAY(180);      
	SCK_HIGH();
	USDELAY(180);
	SCK_LOW();    
	USDELAY(180); 
	SDA_HIGH();
	USDELAY(180);
}


static void i2cm_nack ( void )
{	
	SDA_OUT();
	SDA_HIGH(); 
	USDELAY(180);      
	SCK_HIGH();
	USDELAY(180);
	SCK_LOW(); 
}


void I2CM_Init(void){
	SCK_OUT();	
	SDA_IN();
}


int I2CM_WriteNBytesFrom(uint8_t *src, uint8_t SlaveAddress, uint8_t RegAddress,uint8_t NbrOfBytes){
	int i;
	
	i2cm_start();               
	if(0 == i2cm_send_byte ( SlaveAddress+0 )){//写命令
		goto __fail;
	}

	if(0 == i2cm_send_byte( RegAddress )){//写寄存器地址
		goto __fail;
	}
	
	for(i=0;i<NbrOfBytes;i++){
		if(0 == i2cm_send_byte ( src[i] )){//写N个字节到从机
			goto __fail;
		}
	}
	
	//发送停止位
	i2cm_stop();
	return 0;
	__fail:
	i2cm_stop();
	return -1;

}

int I2CM_ReadNBytesTo(uint8_t *dest, uint8_t SlaveAddress, uint8_t RegAddress,uint8_t NbrOfBytes){
	int i;
	uint8_t temp;
	/* 1. 告诉从机起始寄存器地址 ------------------------------- */
	i2cm_start();               
	if(0 == i2cm_send_byte ( SlaveAddress+0 )){//写命令
		goto __fail;
	}

	if(0 == i2cm_send_byte( RegAddress )){//写寄存器地址
		goto __fail;
	}
	i2cm_stop();
	
	
	/* 2. 向从机读N个字节 -------------------------------------- */
	i2cm_start();			 	//重新发起始信号
	if(0==i2cm_send_byte(SlaveAddress+1)){//读命令
		goto __fail;
	}
	for(i=0;i<NbrOfBytes;i++){
		temp = i2cm_read_byte ();//读取温湿度的高位字节
		if(i+1 < NbrOfBytes){
			i2cm_ack();//mcu应答
		}
		else{
			i2cm_nack();//mcu无应答
		}
		if(dest != 0){
			dest[i] = temp;
		}
	}
	
	i2cm_stop();
	return 0;
	__fail:
	i2cm_stop();
	return -1;
}



