/******************************************************************************
 * FUNCTION_PURPOSE: I/O Pin interrupt Service Routine
 ******************************************************************************/
 #include "N76E003.h"
 #include "SFR_Macro.h"
 #include "Function_Define.h"
 #include "bsp.h"
void EXT_INT0(void) interrupt 0
{
	
		valve_turn_left();
}

		
void EXT_INT1(void) interrupt 2
{
	
		valve_turn_right();
}

		
void PinInterrupt_ISR (void) interrupt 7
{
//	if(PIF == 0x01)
//	{
//    PIF = 0x00;                             //clear interrupt flag
//	}
//	else if (PIF == 0x80)
//	{
//    PIF = 0x00;                             //clear interrupt flag
//	}
//	else{
//		...
//	}
}
