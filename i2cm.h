#ifndef __I2CM_H__
#define __I2CM_H__	 

#include "N76E003.h"
#include "Common.h"
#include "Delay.h"
#include "SFR_Macro.h"
#include "Function_Define.h"
#include "bsp.h"


#define USDELAY(X) 		delayus(X)

#define SCK_OUT()			P17_PushPull_Mode
#define SCK_LOW()			P17 = 0
#define SCK_HIGH()		P17 = 1

#define SDA_IN()			P16_Quasi_Mode
#define READ_SDA()		P16
#define SDA_OUT()			P16_Quasi_Mode
#define SDA_HIGH()		P16 = 1
#define SDA_LOW()			P16 = 0


void I2CM_Init(void);
int I2CM_WriteNBytesFrom(uint8_t *src, uint8_t SlaveAddress, uint8_t RegAddress,uint8_t NbrOfBytes);
int I2CM_ReadNBytesTo(uint8_t *dest, uint8_t SlaveAddress, uint8_t RegAddress,uint8_t NbrOfBytes);


#endif
