#ifndef __BM8563_H__
#define __BM8563_H__




typedef struct{
	unsigned char :3;
	/*
		TESTC=0：电源复位功能失效（普通模式时置为逻辑 0）
		TESTC=1:电源复位功能有效
	*/
	unsigned char TESTC:1;
	unsigned char :1;
	/*
		STOP=0： RTC 时钟运行； STOP=1：所有 RTC 分频器异步置为逻辑 0， RTC 时
		钟停止运行（ CLKOUT 在 32.768kHz 时依然可用）
	*/
	unsigned char STOP:1;
	unsigned char :1;
	/*
		TEST1=0:普通模式
		TEST1=1： EXT_CLK 测试模式
	*/
	unsigned char TEST1:1;
}CTRL_STATUS_REG1_t;

typedef struct{
	/*
		TIE=0：定时器中断被禁止
		TIE=1：定时器中断被使能
	*/
	unsigned char TIE:1;
	/*
		AIE=0：报警中断被禁止
		AIE=1：报警中断被使能
	*/
	unsigned char AIE:1;
	/*
		TF=0：读操作时，定时器标志无效；写操作时，定时器标志被清除
		TF=1：读操作时，定时器标志有效；写操作时，定时器标志保持不变	
	*/
	unsigned char TF:1;
	/*
		AF=0：读操作时，报警标志无效；写操作时，报警标志被清除
		AF=1：读操作时，报警标志有效；写操作时，报警标志保持不变
	*/
	unsigned char AF:1;
	/*
		TI/TP=0：当 TF 有效时， INT 有效（取决于 TIE 的状态）
		TI/TP=1： INT，脉冲有效，见表 5（取决于 TIE 的状态）
		注意：若 AF 和 AIE 都有效时，则 INT 一直有效	
	*/
	unsigned char TI_TP:1;
	unsigned char :3;
}CTRL_STATUS_REG2_t;

typedef struct{
	/*
		用于控制 CLKOUT 的频率输出管脚（ fCLKOUT），见表 20
	*/
	unsigned char FD0:1;
	/*
		用于控制 CLKOUT 的频率输出管脚（ fCLKOUT），见表 20
	*/
	unsigned char FD1:1;
	unsigned char :5;	
	/*
		FE=0： CLKOUT 输出被禁止并设成高阻抗
		FE=1： CLKOUT 输出有效
	*/
	unsigned char FE:1;
	
}CLKOUT_FREQ_CTRL_REG_t;

typedef struct{
	/*
		定时器时钟频率选择位，决定倒计数定时器的时钟频率，见表 22，不用时 TD1
		和 TD0 应设为“11”（ 1/60Hz），以降低电源损耗
	*/
	unsigned char TD0:1;
	unsigned char TD1:1;
	unsigned char :5;
	/*
		TE=0：定时器无效； TE=1：定时器有效
	*/
	unsigned char TE:1;
}TMR_CTRL_REG_t;

typedef struct{
	/*
		倒计数数值“n”，倒计数周期=n/时钟频率
	*/
	unsigned char downcount_value;
}TMR_DOWNCOUNT_VALUE_REG_t;


typedef struct{
	/*
		代表 BCD 格式的当前秒数值，值为 00～99，例如： 1011001 代表 59 秒
	*/
	unsigned char bcd_second:7;
	/*
		VL=0：保证准确的时钟/日历数据
		VL=1：不保证准确的时钟/日历数据		
	*/
	unsigned char VL:1;
}SECOND_REG_t;


typedef struct{
	/*
		代表 BCD 格式的当前分钟数值，值为 00～59
	*/
	unsigned char bcd_minute:7;
	unsigned char :1;
}MINUTE_REG_t;

typedef struct{
	/*
		代表 BCD 格式的当前小时数值，值为 00～23
	*/
	unsigned char bcd_hour:6;
	unsigned char :2;
}HOUR_REG_t;

typedef struct{
	/*
		代表 BCD 格式的当前日数值，值为 01～31。当年计数器的值是闰年是，BD8583
		自动给二月增加一个值，使其成为 29 天
	*/
	unsigned char bcd_day:6;
	unsigned char :2;
}DAY_REG_t;

typedef struct{
	/*
		代表当前星期数值，值为 0～6。见表 11，这些位也可有用户重新分配
	*/
	unsigned char week:3;
	unsigned char :5;
}WEEK_REG_t;

typedef struct{
	/*
		代表 BCD 格式的当前月份数值，值为 01～12，见表 13
	*/
	unsigned char bcd_month:5;
	unsigned char :2;
	/*
		世纪位： C=0 指定世纪数为 20XX； C=1 指定世纪数为 19XX， “XX”为年寄存器
		中的值，见表 14。当年由 99 变为 00 时，世纪为会改变。
	*/
	unsigned char C:1;
}MONTH_CENTURY_t;

typedef struct{
	/*
		代表 BCD 格式的当前年数值，值为 00～99
	*/
	unsigned char bcd_year;
}YEAR_REG_t;

typedef struct{
	/*
		代表 BCD 格式的分钟报警数值，值为 00～59
	*/
	unsigned char bcd_minute:7;
	/*
		AE=0，分钟报警有效； AE=1，分钟报警无效
	*/
	unsigned char AE:1;
}ALARM_MINUTE_REG_t;

typedef struct{
	/*
		代表 BCD 格式的小时报警数值，值为 00～59
	*/
	unsigned char bcd_hour:6;
	unsigned char :1;
	/*
		AE=0，小时报警有效； AE=1，小时报警无效
	*/
	unsigned char AE:1;
}ALARM_HOUR_REG_t;

typedef struct{
	/*
		代表 BCD 格式的日报警数值，值为 00～31
	*/
	unsigned char bcd_day:6;
	unsigned char :1;
	/*
		AE=0，日报警有效； AE=1，日报警无效
	*/
	unsigned char AE:1;
}ALARM_DAY_REG_t;

typedef struct{
	/*
		代表 BCD 格式的星期报警数值，值为 00～59
	*/
	unsigned char bcd_week:3;
	unsigned char :4;
	/*
		AE=0，星期报警有效； AE=1，星期报警无效
	*/
	unsigned char AE:1;
}ALARM_WEEK_REG_t;



#define CTRL_STATUS_REG1_ADDR					0X00
#define CTRL_STATUS_REG2_ADDR					0X01
#define CLKOUT_FREQ_CTRL_REG_ADDR			0X0D
#define TMR_CTRL_REG_ADDR							0X0E
#define TMR_DOWNCOUNT_VALUE_REG_ADDR	0X0F
#define SECOND_REG_ADDR								0X02
#define MINUTE_REG_ADDR								0X03
#define HOUR_REG_ADDR									0X04
#define DAY_REG_ADDR									0X05
#define WEEK_REG_ADDR									0X06
#define MONTH_CENTURY_ADDR						0X07
#define YEAR_REG_ADDR									0X08
#define ALARM_MINUTE_REG_ADDR					0X09
#define ALARM_HOUR_REG_ADDR						0X0A
#define ALARM_DAY_REG_ADDR						0X0B
#define ALARM_WEEK_REG_ADDR						0X0C

#define BM8563_I2C_SLAVE_ADDRESS			0XA2



void RTC_EnableAlarm(void);
void RTC_ClearAlarm(void);
void RTC_PeriodicInterrupt(void);
void RTC_SetHourAlarm(unsigned char hour);
void RTC_SetMinuteAlarm(unsigned char minute);
void RTC_DisplayBuffer(void);

#endif