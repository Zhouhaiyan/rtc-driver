#include "bm8563.h"
#include "i2cm.h"


static CTRL_STATUS_REG1_t CTRL_STATUS_REG1;
static CTRL_STATUS_REG2_t CTRL_STATUS_REG2;


static void my_memset(unsigned char * src, unsigned char value, unsigned int len){
	unsigned int i;
	for(i=0;i<len;i++)
		src[i] = value;
}
static unsigned char int2bcd(unsigned char a){
	unsigned char ge = a%10;
	unsigned char shi = a/10%10;
	a = shi*16 + ge;
	return a;
}
void RTC_SetHourAlarm(unsigned char hour){
	ALARM_HOUR_REG_t ALARM_HOUR_REG;
	my_memset((unsigned char *)&ALARM_HOUR_REG,0,1);
	ALARM_HOUR_REG.AE = 0;//报警使能位，0-使能！。不是1使能，坑啊！！！
	ALARM_HOUR_REG.bcd_hour = int2bcd(hour);
	
	I2CM_WriteNBytesFrom((unsigned char *)&ALARM_HOUR_REG,BM8563_I2C_SLAVE_ADDRESS,ALARM_HOUR_REG_ADDR,1);
}

void RTC_EnableAlarm(void){

	my_memset((unsigned char *)&CTRL_STATUS_REG1,0,1);
	CTRL_STATUS_REG1.STOP = 0;
//	I2CM_WriteNBytesFrom((unsigned char *)&CTRL_STATUS_REG1,BM8563_I2C_SLAVE_ADDRESS,CTRL_STATUS_REG1_ADDR,1);

	my_memset((unsigned char *)&CTRL_STATUS_REG2,0,1);
	CTRL_STATUS_REG2.AIE = 1;
	CTRL_STATUS_REG2.TI_TP = 0;
//	I2CM_WriteNBytesFrom((unsigned char *)&CTRL_STATUS_REG2,BM8563_I2C_SLAVE_ADDRESS,CTRL_STATUS_REG2_ADDR,1);
	
	I2CM_WriteNBytesFrom((unsigned char *)&CTRL_STATUS_REG1,BM8563_I2C_SLAVE_ADDRESS,CTRL_STATUS_REG1_ADDR,2);
	
}

void RTC_ClearAlarm(void){
	CTRL_STATUS_REG2.AF = 0;
	CTRL_STATUS_REG2.TF = 0;
	I2CM_WriteNBytesFrom((unsigned char *)&CTRL_STATUS_REG2,BM8563_I2C_SLAVE_ADDRESS,CTRL_STATUS_REG2_ADDR,1);
}

void RTC_SetMinuteAlarm(unsigned char minute){
	ALARM_MINUTE_REG_t ALARM_MINUTE_REG;
	my_memset((unsigned char *)&ALARM_MINUTE_REG,0,1);
	ALARM_MINUTE_REG.AE = 0;//报警使能位，0-使能！。不是1使能，坑啊！！！
	ALARM_MINUTE_REG.bcd_minute = int2bcd(minute);
	I2CM_WriteNBytesFrom((unsigned char *)&ALARM_MINUTE_REG,BM8563_I2C_SLAVE_ADDRESS,ALARM_MINUTE_REG_ADDR,1);
}


void RTC_PeriodicInterrupt(void){

	TMR_CTRL_REG_t TMR_CTRL_REG;
	TMR_DOWNCOUNT_VALUE_REG_t TMR_DOWNCOUNT_VALUE_REG;
	my_memset((unsigned char *)&TMR_CTRL_REG,0,1);
	TMR_CTRL_REG.TE = 1;
	TMR_CTRL_REG.TD1 = 1;
	TMR_CTRL_REG.TD0 = 1;
	I2CM_WriteNBytesFrom((unsigned char *)&TMR_CTRL_REG,BM8563_I2C_SLAVE_ADDRESS,TMR_CTRL_REG_ADDR,1);

	TMR_DOWNCOUNT_VALUE_REG.downcount_value = 1;
	I2CM_WriteNBytesFrom((unsigned char *)&TMR_DOWNCOUNT_VALUE_REG,BM8563_I2C_SLAVE_ADDRESS,TMR_DOWNCOUNT_VALUE_REG_ADDR,1);

	
}


void RTC_DisplayBuffer(void){
	unsigned char buf[16];
	I2CM_ReadNBytesTo(buf,BM8563_I2C_SLAVE_ADDRESS,CTRL_STATUS_REG1_ADDR,16);
}