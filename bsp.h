#ifndef __BSP_H__
#define __BSP_H__


//#include "Function_Define.h"
//#include "SFR_Macro.h"
/*
??????:
1. P0.2/P0.3/P0.4?LED???*?
2. ??P0.1????PWM???*?
3. ??P1.2???-?????*?
4. ??P1.3???-FIN ???*?
5. ??P1.4???-RIN ???*?
6. ??P3.0/INT0?FKEY???*?
7. ??P1.7/INT1?RKEY???*?
8. P0.6/TXD;P0.7/RXD;P0.6/DnR???*?
*/


#define delay1ms(X)							Timer2_Delay1ms(X)

/* Power Switch? ------------------------------------- */
#define init_power_sw_output()			P15_PushPull_Mode
#define turn_on_power_sw()					P15 = 1
#define turn_off_power_sw()					P15 = 0

/* LED? ------------------------------------- */
#define init_led0_output()			P00_PushPull_Mode
#define turn_on_led0()					P00 = 0
#define turn_off_led0()					P00 = 1

#define init_led1_output()			P10_PushPull_Mode
#define turn_on_led1()					P10 = 0
#define turn_off_led1()					P10 = 1

#define init_led2_output()			P00_PushPull_Mode
#define turn_on_led2()					P00 = 0
#define turn_off_led2()					P00 = 1

/* ??? ------------------------------------ */
//#define init_beep_pwm()					do{\
//																	PWM2_P05_OUTPUT_ENABLE;\
//																	PWM_IMDEPENDENT_MODE;\
//																	PWM_CLOCK_DIV_8;\
//																	PWMPH = 0x07;\
//																	PWMPL = 0xCF;\
//																	set_SFRPAGE;\
//																	PWM2H = 0x03;\
//																	PWM2L = 0xCF;\
//																	clr_SFRPAGE;\
//																	set_LOAD;\
//																}while(0)
//#define start_beep()					do{\
//																PWM2_P05_OUTPUT_ENABLE;\
//																set_PWMRUN;\
//															}while(0)
//#define stop_beep()						do{\
//																clr_PWMRUN;\
//																PWM2_P05_OUTPUT_DISABLE;\
//																P05_PushPull_Mode;\
//																P05 = 1;\
//															}while(0)


#define init_beep_pwm()					do{\
																	P05_PushPull_Mode;\
																	P05 = 1;\
																}while(0)
#define start_beep()					(P05 = 0)
#define stop_beep()						(P05 = 1)


/* ?? -------------------------------------- */
#define init_valve_ctrl_output() P14_PushPull_Mode
#define set_valve_ctrl()				 P14 = 1
#define reset_value_ctrl()			 P14 = 0

#define init_valve_fin_output()  P13_PushPull_Mode
#define set_valve_fin()					 P13 = 1
#define reset_valve_fin()				 P13 = 0

#define init_valve_rin_output()  P12_PushPull_Mode
#define set_valve_rin()					 P12 = 1
#define reset_valve_rin()				 P12 = 0

#define valve_turn_left()				 (P13 = 1,P12 = 0,P14 = 1)
#define valve_turn_right()			 (P13 = 0,P12 = 1,P14 = 1)
#define valve_idle()						 (P13 = 0,P12 = 0,P14 = 0)

#define init_valve_rkey_int()		(IT1 = 1,IE1 = 0,EX1 = 1)
#define init_valve_fkey_int()		(IT0 = 1,IE0 = 0,EX0 = 1)


/* 485 --------------------------------------- */
#define init_uart0(Baudrate)		do{InitialUART0_Timer3(Baudrate);P06_PushPull_Mode;}while(0)
#define uart0_tx(c)							do{Send_Data_To_UART0(c);}while(0)
#define uart0_rx()							Receive_Data_From_UART0()

#define init_nb_pwr_off()				do{P02_PushPull_Mode;P02=1;}while(0)
#define nb_pwr_on()							P02 = 0
#define nb_pwr_off()						P02 = 1

void delayus(unsigned char t);
#endif